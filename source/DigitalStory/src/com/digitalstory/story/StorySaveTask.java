package com.digitalstory.story;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;
import android.util.Xml.Encoding;

import com.digitalstory.Helpers;
import com.digitalstory.StoryActivity;

public class StorySaveTask extends AsyncTask<String, Void, Void>
{

	private Story story;
	private StoryActivity context;
	
	public StorySaveTask(Story currentStory, StoryActivity storyActivity)
	{
		this.story = currentStory;
		this.context = storyActivity;
	}
	
	protected Void doInBackground(String... params)
	{
		String out = params[0];
		
		Log.d("StorySaveTask", "save "+out);
		
		Long start_time = System.currentTimeMillis();
		
		
		try
		{
			BufferedOutputStream bos = new BufferedOutputStream(context.openFileOutput(out, Context.MODE_PRIVATE));
			ZipOutputStream zos = new ZipOutputStream(bos);
			
			//LOOP THROUGH IMAGES
			for(int i=0;i<story.images.size();i++)
			{
				Log.d("StorySaveTask", "saving image "+i);
				StoryImage img = story.images.get(i);
				
				zos.putNextEntry(new ZipEntry("_"+(i+1)+".jpg"));
				
				writeImageIntoStream(img, zos);				
				
				zos.closeEntry();
			}
			
			Log.d("StorySaveTask", "saving story.xml");
			zos.putNextEntry(new ZipEntry("_story.xml"));
			String xml = makeXML();
			Log.d("savet", "" + xml);
			zos.write(xml.getBytes());
			
			zos.closeEntry();
			
			if(story.hasAudio())
			{
				Log.d("StorySaveTask", "saving audio.3gp");
				zos.putNextEntry(new ZipEntry("_audio.3gp"));
				
				BufferedInputStream bis = new BufferedInputStream(context.openFileInput(story.getPrivateAudioFile()));
				byte [] buffer = new byte[2048];
				int bytesRead = -1;
				while((bytesRead = bis.read(buffer, 0, 2048))>-1)
				{
					zos.write(buffer, 0, bytesRead);
				}
				bis.close();
				zos.closeEntry();
			}
			
			zos.flush();
			zos.close();
			bos.close();
			
			
			if(story.images.size()>0)
			{
				Log.d("StorySaveTask", "saving image");
				//write out front image
				BufferedOutputStream oStream = new BufferedOutputStream(context.openFileOutput(out.replace("STORY", "IMG").replace(".zip", ".jpg"), Context.MODE_PRIVATE));
				
				Bitmap b1 = story.images.get(0).smallThumbnail;
				b1.compress(Bitmap.CompressFormat.JPEG, 90, oStream);
				oStream.flush();			
				oStream.close();
				
				Log.d("StorySaveTask", "saved image");
			}

			if(story.wasOpened)
			{
				//STORYxxxxxxxxxx.zip
				//01234
				String oldfilename = story.filename;
				context.deleteFile(oldfilename);
				context.deleteFile(oldfilename.replace("STORY", "IMG").replace(".zip", ".jpg"));				
			}
			synchronized(this){
				notifyAll();
			}
			
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		Long end_time = System.currentTimeMillis();
		
		Log.i("StorySaveTask", "Elapsed time: " + (end_time-start_time));
		
		return null;
	}
	
	protected void onPostExecute(Void v)
	{
		context.progressDialog.dismiss();
		//Helpers.listPrivateStorage(context);
	}
	
	
	private String makeXML()
	{
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		try
		{
			serializer.setOutput(writer);
			serializer.startDocument(Encoding.UTF_8.name(), true);
			
			serializer.startTag("", "story");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			if(story.getDateCreated()!=null)
			{
				serializer.attribute("", "date", ""+sdf.format(story.getDateCreated()));
			}
			else
			{
				serializer.attribute("", "date", ""+sdf.format(new Date()));
			}
			
			serializer.startTag("", "images");
			
			for(int i=0;i<story.images.size();i++)
			{
				serializer.startTag("", "image");				
				serializer.attribute("", "fname", "_"+(i+1)+".jpg");
				serializer.attribute("", "delay", ""+story.getDelays().get(i));
				serializer.endTag("", "image");
			}
			
			serializer.endTag("", "images");
			
			serializer.startTag("", "sound");
			if(story.hasAudio())
			{
				serializer.startTag("", "audio");
				serializer.attribute("", "fname", "_audio.3gp");
				serializer.endTag("", "audio");
			}
			serializer.endTag("", "sound");
			
			serializer.endTag("", "story");
			serializer.endDocument();
			
			return writer.toString();	
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	private void writeImageIntoStream(StoryImage img, ZipOutputStream os)
	{
		
		//GET image URI, this may be private(local) or external(remote)
		Uri iUri = img.remoteUri;
		
		
		BitmapFactory.Options op = new BitmapFactory.Options();
		op.inJustDecodeBounds = true;
		
		try
		{
			//LOAD bounds into op
			BitmapFactory.decodeStream(context.getContentResolver().openInputStream(iUri), null, op);
			
			Bitmap src;
						
			//if the bitmap is smaller than 800x600 or 600x800
			if((op.outHeight<800 && op.outWidth<600) || (op.outHeight<600 && op.outWidth<800))
			{
				src = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(iUri));		
				
			}
			else
			{
				op.inJustDecodeBounds = false;
				op.inSampleSize=1;
				
				if(op.outHeight>op.outWidth)
				{
					op.inSampleSize = (int) Math.ceil(op.outHeight/800.0);
				}
				else
				{
					op.inSampleSize = (int) Math.ceil(op.outWidth/800.0);
				}
					
				src = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(iUri), null, op);
			}
			
			src.compress(Bitmap.CompressFormat.JPEG, 85, os);
			os.flush();
			
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		
	}

}
