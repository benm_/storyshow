package com.digitalstory.story;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.xml.sax.SAXException;

import com.digitalstory.Helpers;
import com.digitalstory.StoryActivity;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;
import android.util.Xml.Encoding;

public class StoryLoadTask extends AsyncTask<String, Integer, Boolean> 
{

	private Story target;
	private StoryActivity context;
	
	public StoryLoadTask(Story currentStory, StoryActivity storyActivity) 
	{
		this.target = currentStory;
		this.context = storyActivity;
	}

	protected Boolean doInBackground(String... params) 
	{
		Log.d("StoryLoadTask","Running background thread");
		
		try 
		{
			target.filename = params[0];	
			target.wasOpened = true;
			
			Log.d("StoryLoader.Load", "attempting unzip " + target.filename);
			
			if(unzipToStorage(target.filename, context))
			{
				Log.d("StoryLoader.Load", "unzip successful");
				
				StoryXMLParser shandler = new StoryXMLParser();			
				
				//LOAD FROM XML INTO STORYHANDLER
				Log.d("StoryLoader.Load", "reading xml");
				
				FileInputStream istream = context.openFileInput("_story.xml");
				Xml.parse(istream, Encoding.UTF_8, shandler);
				istream.close();
				
				Log.d("StoryLoader.Load", "completed reading xml");
				
				while(!context.ready)
				{
					Thread.sleep(100);
				}
				
				Log.d("StoryLoadTask","Ready to load story " + target.getDisplay().getThumbnailSize());
				
				target.setDateCreated(shandler.dateCreated);
				
				if(shandler.audio!=null)target.setPrivateAudioFile(shandler.audio);
				
				for(String s : shandler.images)
				{					
					File f = context.getFileStreamPath(s);
					Log.d("StoryLoader.Load", "Loading "+Uri.fromFile(f).toString());
					target.loadImage(Uri.fromFile(f));
				}
				target.setDelays(shandler.delays);
				
				
				
			}		
			else
			{
				Log.d("StoryLoader.Load", "unzip failed");
			}			
			
			
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		} 
		catch (FileNotFoundException e1) 
		{
			e1.printStackTrace();
		} 
		catch (IOException e1) 
		{
			e1.printStackTrace();
		} 
		catch (SAXException e1) 
		{
			e1.printStackTrace();
		}
		
		return true;
	}

	protected void onPostExecute(Boolean result) 
	{
		 context.progressDialog.dismiss();
		 context.currentStory.setSelected(0, true);
		 context.refreshButtonEnabledness();
	}
	
	/**
	 * Unzip the given zip file into the private storage directory, in preparating for loading/reading.
	 * @param fileURI	The content URI pointing at the zip file
	 * @param c			The Activity Context
	 * @return			A boolean indicating success
	 */
	public boolean unzipToStorage(String filename, Context c)
	{
		//CLEAR ANY EXISTING FILES, TODO: This may be unnecessary, at least its fast
		Helpers.cleanPrivateStorage(c);
		
		try 
		{
			
			//OPEN unzip stream
			InputStream istream = c.openFileInput(filename);
			ZipInputStream unzippedStream = new ZipInputStream(istream);
			
			//UNZIP EACH OBJECT
			ZipEntry entry;
			while((entry = unzippedStream.getNextEntry())!= null)
			{
				//READ FILE FROM STREAM
				FileOutputStream ostream = c.openFileOutput(entry.getName(), Context.MODE_PRIVATE);
				byte[] readBuffer = new byte[2048];
				
				int bytesRead;
				while((bytesRead = unzippedStream.read(readBuffer))>-1)
				{
					ostream.write(readBuffer, 0, bytesRead);
				} 				
				
				//WRITE FILE TO OUTPUT STREAM, FLUSH, CLOSE			
				ostream.flush();
				ostream.close();	
				
				Log.i("StoryLoader", "Extracted: " + entry.getName());
				
			}
			
			
			//CLOSE unzip stream
			unzippedStream.close();
			istream.close();			
			
			//RETURN TRUE if we got here without trouble
			return true;
		} 
		catch (FileNotFoundException e) 
		{
			Log.e("StoryLoader", "" + e.getLocalizedMessage());			
		} 
		catch (IOException e) 
		{
			Log.e("StoryLoader", "" + e.getLocalizedMessage());	
		}		
		
		return false;
	}
}
