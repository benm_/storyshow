package com.digitalstory.story;

import android.graphics.Bitmap;
import android.net.Uri;

public class StoryImage 
{
	public Uri remoteUri;
	public Bitmap smallThumbnail;	
	
	public StoryImage(Uri u, Bitmap thumb)
	{
		remoteUri = u;
		smallThumbnail = thumb;
	}
	
	public String toString()
	{
		return "["+remoteUri.toString()+"]";
	}
	
}
