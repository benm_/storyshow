package com.digitalstory.story;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.net.Uri;
import android.util.Log;

public class StoryXMLParser extends DefaultHandler
{
	public List<String> images;
	public List<Integer> delays;
	public String audio;
	public Date dateCreated;
		
	public StoryXMLParser()
	{
		super();
	}
	
	public void startDocument() throws SAXException
	{
		super.startDocument();
		images = new ArrayList<String>();	
		delays = new ArrayList<Integer>();
		dateCreated = new Date();
	}
	
	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException 
	{
			super.startElement(uri, localName, name, attributes);
			 
			//check the localName and determine what to do:
			
			if(localName.equalsIgnoreCase("story"))
			{
				String s = attributes.getValue("date");
				if(s==null) return; //do nothing
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				try 
				{
					dateCreated = sdf.parse(s);
				} 
				catch (ParseException e) 
				{
					Log.e("SAX", "date error: " + sdf); 
				}
			}
			else if(localName.equalsIgnoreCase("image"))
			{
				String s = attributes.getValue("fname");
				images.add(s);	
				
				s = attributes.getValue("delay");
				int delay = 0;
				try 
				{
					delay = Integer.parseInt(s);					
				} 
				catch (NumberFormatException e) 
				{
					Log.e("SAX", "delay error: " + s); 
				}		
				finally
				{
					delays.add(delay);
				}
				
			}
			else if(localName.equalsIgnoreCase("audio"))
			{
				String s = attributes.getValue("fname");
				audio = s;
			}
	}
	
	public void endDocument() throws SAXException
	{
		super.endDocument();		
	}
}
