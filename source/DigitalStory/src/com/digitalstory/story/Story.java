package com.digitalstory.story;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.digitalstory.Helpers;
import com.digitalstory.StoryActivity;
import com.digitalstory.components.StoryImageView;
import com.digitalstory.components.VerticalImageStrip;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

public class Story
{
	public List<StoryImage>		images;
	public int					selected	= -1;
	private String				privateAudioFile;
	private Date				dateCreated;
	public boolean				wasOpened	= false;
	public Uri					srcURI;
	public String				filename;

	// CHILD VIEWS
	private VerticalImageStrip	vFilmStrip;
	private StoryImageView		vCentralImage;

	// PARENT
	private StoryActivity		parent;
	private List<Integer>		delays;

	public Story()
	{
		images = new ArrayList<StoryImage>();
	}

	public Story(StoryXMLParser sh)
	{
		super();
	}

	/**
	 * Link the child views in for future control
	 */
	public void linkViews(VerticalImageStrip films, StoryImageView centr, StoryActivity act)
	{
		this.vFilmStrip = films;
		this.vCentralImage = centr;
		this.parent = act;
	}

	/**
	 * Add image URI and generate thumbnail. Insert at correct position DOES NOT
	 * FIRE ANY invalidation AND DOES NOT CHANGE SELECTED
	 */
	private int addImage(Uri imageUri, int position)
	{
		// first build thumbnail
		try
		{
			Bitmap thumb = Helpers.makeScaledImage(imageUri, 240, 320, vFilmStrip.getContext());
			StoryImage i = new StoryImage(imageUri, thumb);
			// if no images in list or no position given
			if (position == -1 || images.size() == 0)
			{
				images.add(i);
				return images.size() - 1;
			}
			else
			// otherwise add it at the given position error if bad
			{
				images.add(position + 1, i);
				return position + 1;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * Load an Image into the Story Object Used when loading a story from a zip
	 */
	public void loadImage(Uri imageUri)
	{
		addImage(imageUri, -1);
	}

	/**
	 * Add an image to the Story at the given position Used when adding an image
	 * during story creating
	 */
	public void addImageIntoStory(Uri imageUri, int position)
	{
		int r = addImage(imageUri, position);
		if (r == -1) return;
		// TODO: figure out what to do with delays here
		selected = r;
		vFilmStrip.scrollToIndex(selected);
		refreshCenter();
	}

	/**
	 * Increments the currently selected image to the next image
	 */
	public void nextImage()
	{
		Log.i("story.java", "next image");

		if (images.size() > selected+1)
			setSelected(selected+1, true);
	}

	/**
	 * Decrements the currently selected image to the previous image
	 */
	public void prevImage() {
		Log.i("story.java", "previous image");

		if (selected > 0)
			setSelected(selected-1, true);
	}
	/**
	 * Set the selected index of the filmstrip start scrolling to the position
	 * set the image
	 */
	public void setSelected(int i, boolean loadCenter)
	{
		Log.i("story.java", "setting selected to " + i);
		if (selected == i) return;
		selected = i;
		vFilmStrip.scrollToIndex(selected);
		if (loadCenter)
		{
			refreshCenter();
		}
	}

	public int getSelected()
	{
		return selected;
	}

	public void refreshCenter()
	{
		Log.i("story.java", "refreshing center");
		if(images.size() > 0) vCentralImage.setimg(images.get(selected));
		parent.refreshButtonEnabledness();

	}
	
	public Boolean hasAudio()
	{
		return privateAudioFile!=null;
	}

	public String getPrivateAudioFile()
	{
		return privateAudioFile;
	}

	public void setPrivateAudioFile(String privateAudioFile)
	{
		this.privateAudioFile = privateAudioFile;
	}

	public VerticalImageStrip getDisplay()
	{
		return vFilmStrip;
	}

	public Date getDateCreated()
	{
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated)
	{
		this.dateCreated = dateCreated;
	}

	public void removeImage(int index)
	{

		// if the index is in range:
		if (index >= 0 && index < images.size())
		{
			// remove it
			images.remove(index);
			// remove the delays associated with the image
			if(delays.size() > index) delays.remove(index);
			// was it the selected item
			if (index == selected)
			{
				// if there are now no images left:
				if (images.size() == 0)
				{
					selected = -1;
					vCentralImage.setimg(null);
					parent.refreshButtonEnabledness();
				}
				else
				{
					if (selected == images.size())
					{
						selected -= 1;
					}

					refreshCenter();
				}

			}

		}
	}

	public void moveSelectedImageBack()
	{
		if (selected > 0)
		{
			StoryImage i = images.get(selected);
			images.remove(selected);
			images.add(selected - 1, i);

			/*Integer j = delays.get(selected);
			delays.remove(selected);
			delays.add(selected - 1, j);*/

			setSelected(selected - 1, false);
			parent.refreshButtonEnabledness();

		}
	}

	public void moveSelectedImageForward()
	{
		if (selected < (images.size() - 1))
		{
			StoryImage i = images.get(selected);
			images.remove(selected);
			images.add(selected + 1, i);

			/*Integer j = delays.get(selected);
			delays.remove(selected);
			delays.add(selected + 1, j);*/

			setSelected(selected + 1, false);
			parent.refreshButtonEnabledness();
		}
	}

	public void setDelays(List<Integer> delays)
	{
		this.delays = delays;
	}

	public List<Integer> getDelays()
	{
		return this.delays;
	}
	public void addDelay(Integer i){
		this.delays.add(i);
	}

}
