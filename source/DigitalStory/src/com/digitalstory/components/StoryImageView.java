package com.digitalstory.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import com.digitalstory.Helpers;
import com.digitalstory.story.Story;
import com.digitalstory.story.StoryImage;

public class StoryImageView extends ImageView {
	private Story linkedStory;
	public boolean touchEnabled;
	public StoryImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		touchEnabled = true;
	}

	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}

	public void setimg(StoryImage storyImage) {
		Log.d("setimg", "" + storyImage);
		if (storyImage == null) {
			setImageBitmap(null);
			return;
		}

		try {
			Bitmap scaled = Helpers.makeFitInput(storyImage.remoteUri,
					this.getWidth(), this.getHeight(), this.getContext());
			setImageBitmap(scaled);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void linkToStory(Story src) {
		linkedStory = src;
	}

}
