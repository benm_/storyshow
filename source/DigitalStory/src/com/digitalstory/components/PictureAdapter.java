package com.digitalstory.components;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.digitalstory.R;

/**
 * A class to display stories on the launcher screen in a list view.
 * 
 * This is populated by MainActivity using StoryItems. It simply loads the
 * images when needed.
 */
public class PictureAdapter extends ArrayAdapter<StoryItem>
{
	Context	context;

	public PictureAdapter(Context context, int resourceId, List<StoryItem> items)
	{
		super(context, resourceId, items);
		this.context = context;
	}

	/* private view holder class */
	private class ViewHolder
	{
		ImageView	imageView;
	}

	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder = null;
		StoryItem rowItem = getItem(position);

		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.story_item, null);
			holder = new ViewHolder();
			holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
			convertView.setTag(holder);
		}
		else 
		{
			holder = (ViewHolder) convertView.getTag();
		}
		holder.imageView.setImageBitmap(rowItem.getImage());
		return convertView;
	}
}
