package com.digitalstory.components;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Basically just a wrapper class for items displayed on the launcher screen.
 * 
 * newStory signals that a new story is to be created, openStory indicated that
 * the user wants to select an existing story on their SD card
 */
public class StoryItem implements Comparable<StoryItem>
{
	private String		zipLocation;
	private Bitmap	image;
	private boolean	newStory;

	public StoryItem(String filename, Bitmap image)
	{
		this(filename, image, false);
	}

	public StoryItem(String filename, Bitmap image, boolean newImage)
	{
		this.zipLocation = filename;
		this.image = image;
		this.setNewStory(newImage);
	}

	public Bitmap getImage()
	{
		return image;
	}

	public void setImage(Bitmap b)
	{
		this.image = b;
	}

	public String getZipLocation()
	{
		return zipLocation;
	}

	public void setZipLocation(String location)
	{
		this.zipLocation = location;
	}

	public boolean isNewStory()
	{
		return newStory;
	}

	public void setNewStory(boolean newImage)
	{
		this.newStory = newImage;
	}

	public int compareTo(StoryItem another)
	{
		if(newStory)return -1;
		if(another.newStory)return 1;		
		return this.zipLocation.compareTo(another.zipLocation)*-1;
		
	}
	
	
}
