package com.digitalstory.components;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.digitalstory.Helpers;
import com.digitalstory.R;
import com.digitalstory.story.Story;

/**
 * Vertical ImageStrip component
 * -----------------------------
 * Loads image data from a list of image URI's
 * exposes methods to get the current selected URI and length etc..
 */
public class VerticalImageStrip extends View implements OnTouchListener
{
	
	/** Size of the View */
	private Rect selfSize = new Rect();	
	/** Size of a single image thumbnail */
	private Rect squareSize = new Rect();		
	
	private int scrollY = 0;
	private boolean scrolling = false;
	private int targetScrollY = 0;
	
	private Paint solidPaint = new Paint();
	private Paint linePaint = new Paint();
	
	private final GradientDrawable potentialUpScroll = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0xff000000, 0x00000000});
	private final GradientDrawable potentialDownScroll = new GradientDrawable(Orientation.BOTTOM_TOP, new int[]{0xff000000, 0x00000000});
	
	private Story linkedStory = new Story();
	
	private int selectedColor;
	
	public boolean touchEnabled = true;
	

	public VerticalImageStrip(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		setOnTouchListener(this);
		linePaint.setColor(Color.WHITE);
		linePaint.setStyle(Style.STROKE);
		linePaint.setStrokeWidth(2);		
		
		selectedColor = context.getResources().getColor(R.color.actionbar_background_start);
	}
	
	public void linkToStory(Story src)
	{
		linkedStory = src;
	}
	
	/**
	 * onMeasure() called when component is resized
	 * stores the size of the component itself in selfSize
	 */
	public void onMeasure(int widthSpec, int heightSpec)
	{
		
		int w = MeasureSpec.getSize(widthSpec);
		int h = MeasureSpec.getSize(heightSpec);
				
		selfSize.set(0,0,w, h);								//size of view		
		squareSize.set(0,0,w, w);							//size of a widthxwidth square
		potentialUpScroll.setBounds(0, 0, w, w/4);			//position and size of the top scroll halo
		potentialDownScroll.setBounds(0, h-w/4, w, h);		//position and size of the bottom scroll halo
		
		super.onMeasure(widthSpec, heightSpec);		
	}
	
	/**
	 * onDraw() draws the component. Called automatically when the component requires repainting.
	 * force this by calling invalidate()
	 */
	public void onDraw(Canvas canvas)
	{
		//fill background
		solidPaint.setColor(Color.LTGRAY);
		canvas.drawRect(selfSize, solidPaint);
		
		
		Rect tR = new Rect(squareSize);
		tR.offset(0, -scrollY);
		

		solidPaint.setColor(Color.WHITE);	
		if(touchEnabled)
		{
			solidPaint.setAlpha(255);
		}
		else
		{
			solidPaint.setAlpha(150);
		}
		for(int i=0;i<linkedStory.images.size();i++)
		{		
			canvas.drawBitmap(linkedStory.images.get(i).smallThumbnail, null, tR, solidPaint);
			
			
			if(i==linkedStory.selected)
			{
				linePaint.setColor(Color.WHITE);
				linePaint.setStrokeWidth(2);
				canvas.drawRect(tR.left+1, tR.top+1, tR.right-1, tR.bottom-1, linePaint);
				
				linePaint.setColor(selectedColor);
				linePaint.setStrokeWidth(1);
				canvas.drawRect(tR.left, tR.top, tR.right-1, tR.bottom-1, linePaint);
			}			
			
			//offset the temp rectangle
			tR.offset(0, tR.height());
			
		}
		
		
		if(scrollY>0)
		{
			potentialUpScroll.draw(canvas);
		}
		
		if((selfSize.height()+scrollY)<(linkedStory.images.size()*tR.height()))
		{
			potentialDownScroll.draw(canvas);
		}
	}

	/**
	 * onTouch() event called by ANY touch events, down, move, up etc..
	 */
	private long downTime;
	public boolean onTouch(View v, MotionEvent event) 
	{
		if(touchEnabled)
		{
			if(event.getAction() == MotionEvent.ACTION_DOWN)
			{
				downTime = System.currentTimeMillis();
				int yp = (int)((event.getY() + scrollY)/squareSize.height());
				linkedStory.setSelected(Helpers.clamp(yp, 0, linkedStory.images.size()-1), true);					
			}
			else if(event.getAction() == MotionEvent.ACTION_MOVE && (System.currentTimeMillis()-downTime)>500)
			{			
				int yp = (int)((event.getY() + scrollY)/squareSize.height());
				linkedStory.setSelected(Helpers.clamp(yp, 0, linkedStory.images.size()-1), false);					
			}			
			else if(event.getAction() == MotionEvent.ACTION_UP)
			{
				linkedStory.refreshCenter();
			}
		}
		return true;
	}
	
	/**
	 * Start scrolling towards a target image 
	 */
	public void scrollToIndex(int i)
	{
		targetScrollY = ((squareSize.height()-selfSize.height())>>1) + i*squareSize.height()+i;
		
		if(!scrolling)
		{
			threadHandler.post(scollBackThread);
			scrolling = true;
		}
	}
	
		
	public int getThumbnailSize()
	{
		if(squareSize.width()==0)return 10;
		return squareSize.width();
	}
	
	
	/**
	 * Threads for scrolling the view to the target
	 * threadHandler - for queueing runnables
	 * scrollBackThread - the actual runnable that performs the scroll
	 */
	final Handler threadHandler = new Handler();	
	final Runnable scollBackThread = new Runnable()
	{		
		public void run() 
		{
			//if not at target position
			if(scrollY!=targetScrollY)
			{
				if(scrollY<targetScrollY)
				{
					scrollY += (targetScrollY-scrollY)/6;
				}
				else
				{
					scrollY -= (scrollY-targetScrollY)/6;
				}
				
				//if we are withing range of the target: jump to target and stop
				if(Math.abs(scrollY-targetScrollY)<3)
				{
					scrollY=targetScrollY; 					
					scrolling = false;
				}
				else
				{
					//more SCROLL is required.
					threadHandler.postDelayed(this, 10);
				}
				invalidate();								//redraw
			}	
		}			
	};
	
	
	

}
