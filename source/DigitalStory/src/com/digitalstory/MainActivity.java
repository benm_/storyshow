package com.digitalstory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.digitalstory.components.PictureAdapter;
import com.digitalstory.components.StoryItem;
import com.markupartist.android.widget.ActionBar;

public class MainActivity extends Activity implements OnItemClickListener
{

	final int		INTENT_IMPORTFILE	= 1;
	ActionBar		actionbar;
	GridView		storyListView;
	PictureAdapter	storyAdapter;
	
	List<StoryItem>	storyItems;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		actionbar = (ActionBar) findViewById(R.id.actionBar1);
		actionbar.setTitle("Digital Story");
		
		storyItems = new ArrayList<StoryItem>();	
				
		storyListView = (GridView) findViewById(R.id.gridView1);
		registerForContextMenu(storyListView);
		
		storyAdapter = new PictureAdapter(this, R.layout.story_item, storyItems);
		storyListView.setAdapter(storyAdapter);
		storyListView.setOnItemClickListener(this);
		
		
	}
	
	protected void onResume()
	{
		super.onResume();
		refreshStoryList();
	}
	

	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.menu_main_clear_storage:
				Helpers.clearPrivateStorageCompletely(this);
				refreshStoryList();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) {  
		super.onCreateContextMenu(menu, v, menuInfo);  
		menu.setHeaderTitle("Context Menu");  
		menu.add(0, v.getId(), 0, "Delete");  
	}  
	
	public boolean onContextItemSelected(MenuItem item) {  
		Log.d("context", ""+item.getTitle());
		
	    if(item.getTitle().equals("Delete"))
	    {
	    	AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
	    	
	    	StoryItem img = storyItems.get(info.position);
	    	
	    	if(img.isNewStory())return false;	    	
	    	
	    	this.deleteFile(img.getZipLocation());
	    	this.deleteFile(img.getZipLocation().replace("STORY", "IMG").replace(".zip", ".jpg"));
	    	this.refreshStoryList();
	    	
	    }  
	    else 
	    {
	    	return false;
	    }  
	    return true;  
	}  
	
	
	

	public void newStoryClick(View view)
	{
		Intent intent = new Intent(this, StoryActivity.class);
		startActivityForResult(intent, 0);
	}


	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		StoryItem item = storyItems.get(position);
		if (item.isNewStory())
		{ 
			// make new story
			Intent intent = new Intent(this, StoryActivity.class);
			startActivityForResult(intent, 0);
		}
		else
		{ 
			// open story from location
			String filename = storyItems.get(position).getZipLocation();
			Log.i("openfile", "" + filename);
			Intent intent = new Intent(this, StoryActivity.class);
			intent.putExtra("fileURI", filename);
			startActivityForResult(intent, 0);
		}
	}

	public void refreshStoryList()
	{
		storyItems.clear();
		
		//add new story button
		StoryItem item = new StoryItem(null, BitmapFactory.decodeResource(getResources(), R.drawable.ic_menu_add), true);
		storyItems.add(item);
		
		//get all filename
		for(String filename : this.fileList())
		{
			//is STORYxxxxxxx.zip
			if(filename.startsWith("STORY") && filename.endsWith(".zip"))
			{
				Bitmap img = BitmapFactory.decodeResource(getResources(), R.drawable.aplay);

				File f = getFileStreamPath(filename.replace("STORY", "IMG").replace(".zip", ".jpg"));
				Bitmap test = Helpers.makeFitInput(Uri.fromFile(f), 50, 50, this);
				
				if(test!=null)
				{
					img=test;
				}
				
				StoryItem si = new StoryItem(filename, img);
				storyItems.add(si);				
			}
		}
		
		Collections.sort(storyItems);
		
		storyAdapter.notifyDataSetChanged();	
		
		
	}


}
