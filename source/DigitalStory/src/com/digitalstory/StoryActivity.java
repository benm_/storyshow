package com.digitalstory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater.Filter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.digitalstory.components.StoryImageView;
import com.digitalstory.components.VerticalImageStrip;
import com.digitalstory.story.Story;
import com.digitalstory.story.StoryLoadTask;
import com.digitalstory.story.StorySaveTask;
import com.markupartist.android.widget.ActionBar;

public class StoryActivity extends Activity implements OnClickListener {

	public VerticalImageStrip imageStrip;
	public StoryImageView imageView;
	public Story currentStory;
	public boolean ready = false;
	public ProgressDialog progressDialog;
	private ActionBar actionbar;

	private final int IMAGE_INTENT = 1001;
	private Uri newPhotoUri;

	private enum MenuMode {
		MAIN, REORDER, MANAGEAUDIO, RECORDSTORY, PLAYSTORY
	};

	private MenuMode menuMode = MenuMode.MAIN;

	private LinearLayout menubar1;
	private LinearLayout menubar2;
	private LinearLayout menubar_reorder;
	private LinearLayout menubar_manage_audio;
	private LinearLayout menubar_recording;

	private ImageButton btnReorder;
	private ImageButton btnRemove;
	private ImageButton btnMoveBack;
	private ImageButton btnMoveForward;

	// MEDIA RECORDING AND PLAYING CLASSES
	private MediaPlayer mPlayer;
	private MediaRecorder mRecorder;
	private boolean recordingStory;
	private boolean playingAudio;
	private boolean playingStory;
	private ScheduledThreadPoolExecutor stpe;
	private int startingTime;
	private int totalDelays;

	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private GestureDetector gestureDetector;
	View.OnTouchListener gestureListener;

	/**
	 * OnActivityCreate
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_story);

		actionbar = (ActionBar) findViewById(R.id.actionBar2);
		actionbar.setTitle("Digital Story");

		menubar1 = (LinearLayout) findViewById(R.id.mainButtonBar1);
		menubar2 = (LinearLayout) findViewById(R.id.mainButtonBar2);
		menubar_reorder = (LinearLayout) findViewById(R.id.reorderButtonBar1);
		menubar_manage_audio = (LinearLayout) findViewById(R.id.recordAudioButtonBar1);
		menubar_recording = (LinearLayout) findViewById(R.id.recordingAudioButtonBar1);

		btnReorder = (ImageButton) findViewById(R.id.button_reorder);
		btnRemove = (ImageButton) findViewById(R.id.button_remove_picture);
		btnMoveBack = (ImageButton) findViewById(R.id.button_push_back);
		btnMoveForward = (ImageButton) findViewById(R.id.button_push_forward);

		imageStrip = (VerticalImageStrip) findViewById(R.id.verticalImageStrip1);
		imageView = (StoryImageView) findViewById(R.id.storyImageView1);

		currentStory = new Story();
		currentStory.linkViews(imageStrip, imageView, this);

		imageStrip.linkToStory(currentStory);
		imageView.linkToStory(currentStory);

		gestureDetector = new GestureDetector(new MyGestureDetector());
		gestureListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};

		imageView.setOnClickListener(StoryActivity.this);
		imageView.setOnTouchListener(gestureListener);

		recordingStory = false;
		playingAudio = false;
		playingStory = false;

		// was a filepath passed to the activity?
		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.containsKey("fileURI")) {
			// LOAD STORY
			String fileUri = extras.getString("fileURI");

			// DISPLAY LOADING SCREEN
			progressDialog = ProgressDialog.show(this, "Loading Story",
					"Loading");

			// LOAD STORY IN MULTITHREADED WAY
			StoryLoadTask slt = new StoryLoadTask(currentStory, this);
			slt.execute(fileUri);

		} else {
			// NEW STORY
		}

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_story, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_story_save:
			startSave();
			return true;
		case R.id.menu_story_exit:
			reallyGoBack();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// adapted from
	// http://stackoverflow.com/questions/937313/android-basic-gesture-detection/938657#938657
	// courtesy of gav and obrok
	class MyGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				if (imageView.touchEnabled) {
					if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
						return false;
					// right to left swipe
					if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
							&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
						Log.i("swiping", "swipe left");

						runOnUiThread(new Runnable() {
							public void run() {
								currentStory.nextImage();
							}
						});
					} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
							&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
						runOnUiThread(new Runnable() {
							public void run() {
								currentStory.prevImage();
							}
						});
						Log.i("swiping", "swipe right");
					}
				}
			} catch (Exception e) {
				// nothing
			}
			return false;
		}
	}

	/**
	 * Once the gui has been fully constructed and measured. Set activity to
	 * READY
	 */
	public void onStart() {
		super.onStart();
		ready = true;
	}

	/**
	 * Create and launch a image selector intent.
	 * 
	 * @param view
	 *            - the button firing this action
	 */
	public void pickPhoto(View view) {
		// Generate savefile filename
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
		String fileName = "IMG" + f.format(new Date()) + ".jpg";

		// Construct URI
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		newPhotoUri = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

		// Create gallery picker intent
		Intent pickFromGallery = new Intent();
		pickFromGallery.setType("image/*");
		pickFromGallery.setAction(Intent.ACTION_GET_CONTENT);

		// Create camera intent
		Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePhoto.putExtra(MediaStore.EXTRA_OUTPUT, newPhotoUri);

		// Create image source chooser
		Intent chooser = Intent.createChooser(pickFromGallery, "Choose Source");
		chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS,
				new Intent[] { takePhoto });

		// spawn
		startActivityForResult(chooser, IMAGE_INTENT);
	}

	/**
	 * onActivityResult() Fired when an intent returns to this activity
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == IMAGE_INTENT && resultCode == Activity.RESULT_OK) {
			if (data != null) // is a selected URI attached?
			{
				Uri selectedImage = data.getData();
				Log.d("URI", "uri: " + selectedImage.toString());
				currentStory.addImageIntoStory(selectedImage,
						currentStory.selected);
			} else {
				Uri photoImage = newPhotoUri;
				Log.d("URI", "uri: " + photoImage.toString());
				currentStory.addImageIntoStory(photoImage,
						currentStory.selected);
			}

		}
	}

	// Run when user presses record button
	public void toggleRecordingStory(View view) {
		ImageButton record = (ImageButton) findViewById(R.id.button_record_audio);
		if (!recordingStory) {
			goToRecordingMode(null);
			runOnUiThread(new Runnable() {
				public void run() {
					currentStory.setSelected(0, true);
				}
			});
			startRecording();
		} else {
			record.setImageResource(R.drawable.record);
			goToRecordMode(null);
			stopRecording();
		}
		recordingStory = !recordingStory;
	}

	// Action that occurs when you press
	public void togglePlayingAudio(View view) {
		ImageButton voice_play = (ImageButton) findViewById(R.id.button_play_audio);
		ImageButton[] otherButtons = new ImageButton[] {
				(ImageButton) findViewById(R.id.button_done_recording),
				(ImageButton) findViewById(R.id.button_record_audio) };

		if (!playingAudio) {
			voice_play.setImageResource(R.drawable.stop);
			for (ImageButton b : otherButtons)
				b.setVisibility(View.GONE);
			playAudio();
		} else {
			voice_play.setImageResource(R.drawable.play);
			for (ImageButton b : otherButtons)
				b.setVisibility(View.VISIBLE);
			stopPlayingAudio();
		}
		playingAudio = !playingAudio;
	}

	public void togglePlayingStory(View view) {
		ImageButton story_play = (ImageButton) findViewById(R.id.button_play_story);
		ImageButton[] otherButtons = new ImageButton[] {
				(ImageButton) findViewById(R.id.button_record),
				(ImageButton) findViewById(R.id.button_reorder),
				(ImageButton) findViewById(R.id.button_add_picture),
				(ImageButton) findViewById(R.id.button_remove_picture) };
		if(currentStory != null && currentStory.getDelays() != null){
		if (!playingStory) {
			story_play.setImageResource(R.drawable.pstop);
			for (ImageButton b : otherButtons)
				b.setVisibility(View.GONE);
			playStory();
			imageView.touchEnabled = false;
		} else {
			story_play.setImageResource(R.drawable.aplay);
			for (ImageButton b : otherButtons)
				b.setVisibility(View.VISIBLE);
			stopPlayingStory();
			imageView.touchEnabled = true;
		}
		}
		playingStory = !playingStory;
	}

	// A class used with the scheduled thread pool executor to play stories
	private class IncrementPictureWithDelay implements Runnable {
		private int index;

		public IncrementPictureWithDelay(int index) {
			this.index = index;
		}

		public void run() {
			runOnUiThread(new Runnable() {
				public void run() {
					currentStory.nextImage();
				}
			});
			List<Integer> delays = currentStory.getDelays();
			if (index < delays.size())
				stpe.schedule(new IncrementPictureWithDelay(++index),
						delays.get(index), TimeUnit.MILLISECONDS);
			else
				togglePlayingStory(null);
		}
	}

	public void playStory() {
		if (currentStory != null && currentStory.getDelays() != null) {
			
			Log.i("delay_in_story", "PRINTING DELAYS\n----------------");
			int j = 0;
			for (Integer i : currentStory.getDelays()) {
				Log.i("delay_in_story", "Delay number " + j++ + ": " + i);
			}
			runOnUiThread(new Runnable() {
				public void run() {
					currentStory.setSelected(0, true);
				}
			});
			stpe = new ScheduledThreadPoolExecutor(5);
			if (currentStory.getDelays().size() > 1) {
				stpe.schedule(new IncrementPictureWithDelay(1), currentStory
						.getDelays().get(1), TimeUnit.MILLISECONDS);
			}
			playAudio();
		}
	}

	public void stopPlayingStory() {
		stopPlayingAudio();
		if (stpe != null) {
			stpe.shutdownNow();
			stpe = null;
		}
	}

	// Bottom are all to facilitate recording/playing of audio
	// Based off of
	// http://developer.android.com/guide/topics/media/audio-capture.html
	private void playAudio() {

		mPlayer = new MediaPlayer();
		try {
			String audioFilename = currentStory.getPrivateAudioFile();

			if (audioFilename != null) {
				FileInputStream audioFile = openFileInput(currentStory
						.getPrivateAudioFile());
				mPlayer.setDataSource(audioFile.getFD());

				mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					public void onCompletion(MediaPlayer mp) {
						if (playingAudio)
							togglePlayingAudio(null);
						if (playingStory)
							togglePlayingStory(null);
					}
				});

				mPlayer.prepare();
				mPlayer.start();
			}
		} catch (IOException e) {
			Log.e("audio_playing",
					"prepare() failed" + currentStory.getPrivateAudioFile());
		}

	}

	private void stopPlayingAudio() {
		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}

	private void startRecording() {
		// move to first image
		currentStory.setSelected(0, true);

		// Setup media recorder
		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

		// try to set output file stream
		try {
			if (currentStory.getPrivateAudioFile() == null) {
				currentStory.setPrivateAudioFile("_audio.3gp");
			}

			mRecorder.setOutputFile(openFileOutput(
					currentStory.getPrivateAudioFile(), Context.MODE_PRIVATE)
					.getFD());

			mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			try {
				mRecorder.prepare();
			} catch (IOException e) {
				Log.e("audio_recording", "prepare() failed");
			}
			currentStory.setDelays(new ArrayList<Integer>());
			currentStory.addDelay(0);
			startingTime = (int) System.currentTimeMillis();
			totalDelays = 0;
			mRecorder.start();

		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	private void stopRecording() {
		mRecorder.stop();
		mRecorder.reset();
		mRecorder.release();
		mRecorder = null;
	}

	/**
	 * Delete the currently selected image from the story
	 * 
	 * @param view
	 *            - the button firing this action
	 */
	public void removePhoto(View view) {
		currentStory.removeImage(currentStory.selected);
	}

	public void goToReorderMode(View view) {
		if (currentStory.images.size() < 2)
			return; // bail if no reordering can
					// be done

		menubar1.setVisibility(View.GONE);
		menubar2.setVisibility(View.GONE);
		menubar_reorder.setVisibility(View.VISIBLE);
		menuMode = MenuMode.REORDER;
		refreshButtonEnabledness();
	}

	public void goToRecordMode(View view) {
		if (currentStory.selected == -1)
			return; // bail if no reordering can be
					// done

		menubar1.setVisibility(View.GONE);
		menubar2.setVisibility(View.GONE);
		menubar_recording.setVisibility(View.GONE);
		menubar_manage_audio.setVisibility(View.VISIBLE);
		menuMode = MenuMode.MANAGEAUDIO;
		refreshButtonEnabledness();
		imageStrip.touchEnabled = false;
		imageView.touchEnabled = false;
	}

	public void goToRecordingMode(View view) {
		if (currentStory.selected == -1)
			return; // bail if no reordering can be
					// done

		menubar_manage_audio.setVisibility(View.GONE);
		menubar_recording.setVisibility(View.VISIBLE);

		menuMode = MenuMode.RECORDSTORY;
		refreshButtonEnabledness();
		imageStrip.touchEnabled = false;
		imageView.touchEnabled = false;
	}

	public void moveImageBack(View v) {
		currentStory.moveSelectedImageBack();
	}

	public void moveImageForward(View v) {
		currentStory.moveSelectedImageForward();
	}

	public void goToMainMode(View v) {
		if (menuMode == MenuMode.REORDER) {
			menubar_reorder.setVisibility(View.GONE);
			menubar1.setVisibility(View.VISIBLE);
			menubar2.setVisibility(View.VISIBLE);
			menuMode = MenuMode.MAIN;
		}
		if (menuMode == MenuMode.MANAGEAUDIO) {
			menubar_manage_audio.setVisibility(View.GONE);
			menubar1.setVisibility(View.VISIBLE);
			menubar2.setVisibility(View.VISIBLE);
			menuMode = MenuMode.MAIN;

		}
		refreshButtonEnabledness();
		imageStrip.touchEnabled = true;
		imageView.touchEnabled = false;

	}

	public void refreshButtonEnabledness() {
		Log.d("ButtonRefresh", "ButtonRefresh");
		switch (menuMode) {
		case MAIN:

			if (currentStory.selected != -1) {
				if (currentStory.images.size() > 1) {
					btnReorder.setEnabled(true);
				} else {
					btnReorder.setEnabled(false);
				}

				btnRemove.setEnabled(true);
			} else {
				btnReorder.setEnabled(false);
				btnRemove.setEnabled(false);
			}

			break;
		case REORDER:

			if (currentStory.selected == 0) {
				btnMoveBack.setEnabled(false);
			} else {
				btnMoveBack.setEnabled(true);
			}

			if (currentStory.selected == (currentStory.images.size() - 1)) {
				btnMoveForward.setEnabled(false);
			} else {
				btnMoveForward.setEnabled(true);
			}

			break;
		case RECORDSTORY:
			if (currentStory.selected == (currentStory.images.size() - 1)) {
				((ImageButton) findViewById(R.id.button_advance_story))
						.setEnabled(false);
			} else {
				((ImageButton) findViewById(R.id.button_advance_story))
						.setEnabled(true);
			}
			break;

		default:
			break;
		}
	}

	public void startSave() {
		startSave(false);
	}

	public void startSave(boolean goBack) {
		Log.d("startSave", "pressed");

		if (currentStory.images.size() > 0 || currentStory.hasAudio()) {
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String filename = "STORY" + sdf.format(now) + ""
					+ (int) (Math.random() * 10) + ".zip";

			Log.d("startSave", "will save as " + filename);

			progressDialog = ProgressDialog.show(this, "Saving Story",
					"Saving...");

			StorySaveTask sst = new StorySaveTask(currentStory, this);
			sst.execute(filename);
			if (goBack) {
				synchronized (sst) {
					try {
						sst.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				reallyGoBack();
			}

		} else {
			Toast.makeText(this, "There is nothing to save!",
					Toast.LENGTH_SHORT).show();
		}

	}

	public void onClick(View v) {
		Filter f = (Filter) v.getTag();
	}

	// TODO: stop swiping here. Or make it the only way to work
	public void advanceStory(View view) {
		if (currentStory.selected == currentStory.images.size() - 1)
			return; // bail if over

		int currentTime = (int) System.currentTimeMillis();
		int currentDelay = currentTime - startingTime - totalDelays;
		currentStory.addDelay(currentDelay);
		totalDelays += currentDelay;
		runOnUiThread(new Runnable() {
			public void run() {
				currentStory.nextImage();
			}
		});
	}

	public void onBackPressed() {
		if (menuMode == MenuMode.REORDER) {
			goToMainMode(null);
			return;
		} else if (menuMode != MenuMode.MAIN) {
			return;
		}

		// is there anything to save?
		if (currentStory.images.size() == 0 && !currentStory.hasAudio()) {
			super.onBackPressed();
		}

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					startSave(true);
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					reallyGoBack();
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Would you like to save?")
				.setPositiveButton("Save", dialogClickListener)
				.setNegativeButton("Exit", dialogClickListener).show();

	}

	public void reallyGoBack() {
		super.onBackPressed();
	}

}
