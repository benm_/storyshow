package com.digitalstory;

import java.io.FileNotFoundException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

public class Helpers 
{
	/**
	* clamp( int , int , int)
	* clamp a value to a range. 
	*/
	public static int clamp(int i, int min, int max)
	{
		if(i<min)return min;
		if(i>max)return max;
		return i;
	}
	
	/**
	* clamp( float , float , float)
	* clamp a value to a range. 
	*/
	public static float clamp(float i, float min, float max)
	{
		if(i<min)return min;
		if(i>max)return max;
		return i;
	}
	
	public static Bitmap makeScaledImage(Uri imageURI, int thumbWidth, int thumbHeight, Context c) throws FileNotFoundException
	{				
		Bitmap src = BitmapFactory.decodeStream(c.getContentResolver().openInputStream(imageURI));						
		Bitmap thumb = Bitmap.createScaledBitmap(src, thumbWidth, thumbHeight, false);
		src.recycle();
		return thumb;		
	}
	
	public static Bitmap makeFitInput(Uri imageURI, int maxW, int maxH, Context c)
	{
		final BitmapFactory.Options op = new BitmapFactory.Options();
		op.inJustDecodeBounds = true;
		try
		{
					
			BitmapFactory.decodeStream(c.getContentResolver().openInputStream(imageURI), null, op);
			
			int h = op.outHeight;
			int w = op.outWidth;
			
			int ss = 1;
			if(w>maxW || h>maxH) //if either dimension is out then resizing is required
			{
				if(w>h)
				{
					ss = Math.round((float)w/(float)maxW);
				}
				else
				{
					ss = Math.round((float)h/(float)maxH);
				}
			}
			
			op.inSampleSize = ss;
			op.inJustDecodeBounds = false; 
			
			Bitmap bitm = BitmapFactory.decodeStream(c.getContentResolver().openInputStream(imageURI), null, op);
			return bitm;
			
		}
		catch (FileNotFoundException e)	{ Log.e("makeFitInput", "File Not Found " + imageURI); }
		
		return null;
	}
	
	public static void clearPrivateStorageCompletely(Context c)
	{
		Long time = System.currentTimeMillis();
		Log.i("clearContextStore", "Clearing " + c.fileList().length + " private files..");
		for(String s : c.fileList())
		{
			c.deleteFile(s);
		}
		Log.d("clearContextStore", "..done[" + (System.currentTimeMillis()-time) + "ms]");
	}
	
	public static void cleanPrivateStorage(Context c)
	{
		Long time = System.currentTimeMillis();
		Log.i("clearContextStore", "Clearing " + c.fileList().length + " private files..");
		for(String s : c.fileList())
		{
			if(s.charAt(0)=='_')c.deleteFile(s);
		}
		Log.d("clearContextStore", "..done[" + (System.currentTimeMillis()-time) + "ms]");
	}
	
	public static void listPrivateStorage(Context c)
	{
		Log.i("listPrivateStorage", "Private Storage:");
		Log.i("listPrivateStorage", "----------");
		for(String s:c.fileList())Log.i("listPrivateStorage", s);
		Log.i("listPrivateStorage", "----------");
	}
	
	
}
